class User {
    constructor(name, surname, nickname, password, age, sex, active) {
        this.name = name;
        this.surname = surname;
        this.nickname = nickname;
        this.password = password;
        this.age = age;
        this.sex = sex;
        this.active = active;
    }
}

// Enable or disable buttons based on form validity
function updateButtonsState() {
    const saveButton = document.getElementById('saveButton');
    const registrationForm = document.getElementById('registrationForm');

    // Check if the form is valid
    if (registrationForm.checkValidity()) {
        saveButton.disabled = false;
        saveButton.classList.remove('disabled');
    } else {
        saveButton.disabled = true;
        saveButton.classList.add('disabled');
    }
}

document.getElementById('registrationForm').addEventListener('input', updateButtonsState);


function saveUser() {
    const name = document.getElementById('name').value;
    const surname = document.getElementById('surname').value;
    const nickname = document.getElementById('nickname').value;
    const password = document.getElementById('password').value;
    const age = parseInt(document.getElementById('age').value);
    const sex = document.getElementById('sex').value;
    const active = document.getElementById('active').checked;

    if (!registrationForm.checkValidity()) {
        alert('Please fill in all required fields.');
        return;
    }

    // Validate password
    if (!isValidPassword(password)) {
        alert('Password must have at least six characters and contain both letters and numbers.');
        return;
    }

    const user = new User(name, surname, nickname, password, age, sex, active);

    // Retrieve existing users from local storage or create an empty array
    const existingUsers = JSON.parse(localStorage.getItem('users')) || [];

    // Add the new user to the array
    existingUsers.push(user);

    // Save the updated array back to local storage
    localStorage.setItem('users', JSON.stringify(existingUsers));

    // Optionally, you can display a success message or perform other actions
    alert('User saved successfully!');
}

function isValidPassword(password) {
    // Check if the password has at least six characters and contains both letters and numbers
    return /^(?=.*[A-Za-z])(?=.*\d).{6,}$/.test(password);
}

function displayUsers() {
    const userListDiv = document.getElementById('userList');

    // Retrieve users from local storage
    const users = JSON.parse(localStorage.getItem('users')) || [];

    if (users.length === 0) {
        userListDiv.innerHTML = '<p>No users to display.</p>';
        return;
    }

    // Display users in HTML
    let userDisplay = '<p>Users:</p><ul>';
    users.forEach((user, index) => {
        userDisplay += `<li>${index + 1}. Name: ${user.name}, Surname: ${user.surname}, Nickname: ${user.nickname}, Age: ${user.age}, Sex: ${user.sex}, Active: ${user.active ? 'Yes' : 'No'}</li>`;
    });
    userDisplay += '</ul>';

    userListDiv.innerHTML = userDisplay;
}

function showRegistrationForm() {
    document.getElementById('registrationForm').style.display = 'block';
    document.getElementById('userList').style.display = 'none';
    document.getElementById('loginForm').style.display = 'none';
}

function showLoginForm() {
    document.getElementById('registrationForm').style.display = 'none';
    document.getElementById('userList').style.display = 'none';
    document.getElementById('loginForm').style.display = 'block';
}

function showUserList() {
    hideForms();
    displayUsers();
    document.getElementById('userList').style.display = 'block';
}

function hideForms() {
    document.getElementById('registrationForm').style.display = 'none';
    document.getElementById('loginForm').style.display = 'none';
    document.getElementById('userList').style.display = 'none';
}

function loginUser() {
    const loginNickname = document.getElementById('loginNickname').value;
    const loginPassword = document.getElementById('loginPassword').value;

    // Retrieve users from local storage
    const users = JSON.parse(localStorage.getItem('users')) || [];

    const user = users.find(u => u.nickname === loginNickname);

    if (user && user.password === loginPassword) {
        alert('Login successful!');
    } else {
        alert('Login failed. Please check your nickname and password.');
    }
}